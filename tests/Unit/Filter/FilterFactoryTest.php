<?php


namespace oat\OneRoster\Tests\Unit\Filter;


use oat\OneRoster\Entity\Organisation;
use oat\OneRoster\Exceptions\UnknownEntityTypeException;
use oat\OneRoster\File\FileHandler;
use oat\OneRoster\Filter\FilterInterface;
use PHPUnit\Framework\TestCase;

class FilterFactoryTest extends TestCase
{

    public function testFilterFactory()
    {
        $obj = new \oat\OneRoster\Filter\FilterFactory(new FileHandler());

        $this->assertInstanceOf(FilterInterface::class, $obj->build(Organisation::getType()));

        $this->expectException(UnknownEntityTypeException::class);
        $obj->build('unknownType');
    }

}
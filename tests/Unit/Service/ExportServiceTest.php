<?php


namespace oat\OneRoster\Tests\Unit\Service;


use Doctrine\Common\Collections\ArrayCollection;
use oat\OneRoster\Service\ExportService;
use oat\OneRoster\Storage\InMemoryStorage;
use oat\OneRoster\Storage\StorageInterface;
use PHPUnit\Framework\TestCase;

class ExportServiceTest extends TestCase
{

    public function testExport()
    {
        $tmpPath = $this->createTempDir();

        $service = ExportService::createFromStorage($this->getStorage(), 'testSystem', 'testCode');
        $service->saveToDirectory($tmpPath);

        $this->assertDirectoryExists($tmpPath);

        var_dump(scandir($tmpPath));

        $this->removeDir($tmpPath);
    }

    protected function getStorage(): StorageInterface
    {
        $data = [
            'classes' => new ArrayCollection([
                'class1' => [
                    'sourcedId' => 'class1',
                    'status' => 'active',
                    'dateLastModified' => '',
                    'title' => 'Class 1',
                    'courseSourcedId' => 'course1',
                    'schoolSourcedId' => 'school1',
                ]
            ]),
            'courses' => new ArrayCollection([
                'course1' => [
                    'sourcedId' => 'course1',
                    'orgSourcedId' => 'school1',
                    'title' => 'Course 1'
                ]
            ]),
            'enrollments' => new ArrayCollection([
                'enrollment1' => [
                    'sourcedId' => 'enrollment1',
                    'classSourcedId' => 'class1',
                    'schoolSourcedId' => 'school1',
                    'userSourcedId' => 'user1',
                    'role' => 'student',
                    'status' => 'active'
                ]
            ]),
            'users' => new ArrayCollection([
                'user1' => [
                    'sourcedId' => 'user1',
                    'enabledUser' => true,
                    'dateLastModified' => false
                ],
                'user2' => [
                    'sourcedId' => 'user2',
                    'enabledUser' => true,
                    'dateLastModified' => new \DateTime()
                ],
            ]),
            'orgs' => new ArrayCollection([
                'school1' => [
                    'sourcedId' => 'school1',
                    'status' => 'active',
                    'name' => 'School 1',
                    'type' => 'school',
                    'identifier' => 'school1'
                ]
            ]),
        ];

        return new InMemoryStorage($data);
    }

    protected function createTempDir()
    {
        $sourceDir = sys_get_temp_dir();

        $attempts = 0;
        $maxAttempts = 100;

        do {
            $path = sprintf('%s%s%s%s', $sourceDir, DIRECTORY_SEPARATOR, 'testExport', mt_rand(100000, mt_getrandmax()));
        } while (
            !mkdir($path) &&
            $attempts++ < $maxAttempts
        );

        return $path;
    }

    protected function removeDir(string $path)
    {
        if (!file_exists($path)) {
            return true;
        }

        if (!is_dir($path)) {
            return unlink($path);
        }

        foreach (scandir($path) as $item) {
            if ($item == '.' || $item == '..') {
                continue;
            }

            if (!$this->removeDir($path . DIRECTORY_SEPARATOR . $item)) {
                return false;
            }
        }

        return rmdir($path);
    }

}
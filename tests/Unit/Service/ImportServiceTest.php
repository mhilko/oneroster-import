<?php

namespace oat\OneRoster\Tests\Service;

use oat\OneRoster\Entity\ClassRoom;
use oat\OneRoster\Entity\Organisation;
use oat\OneRoster\File\FileHandler;
use oat\OneRoster\Service\AbstractImportService;
use oat\OneRoster\Service\ImportService;
use PHPUnit\Framework\TestCase;

class ImportServiceTest extends TestCase
{
    /**
     * @throws \Exception
     */
    public function testImportMultiple()
    {
        $service = new ImportService($this->getFileHandlerMock());

        $result = $service->importMultiple('/path/to/a/folder/');

        $this->assertIsArray($result);
    }

    /**
     * @throws \Exception
     */
    public function testDetectAvailableTypes()
    {
        $service = new ImportService($this->getFileHandlerMock());

        $service->setPathToFolder('/path/to/a/folder/');

        $result = $service->getEntitiesModes();
        $this->assertIsArray($result);

        $this->assertEquals(AbstractImportService::ENTITY_MODE_BULK, $service->getEntityMode(Organisation::getType()));
        $this->assertEquals(AbstractImportService::ENTITY_MODE_ABSENT, $service->getEntityMode(ClassRoom::getType()));
    }

    /**
     * @return FileHandler
     */
    public function getFileHandlerMock(): FileHandler
    {
        $file = $this->getMockBuilder(FileHandler::class)->disableOriginalConstructor()->getMock();

        $file
            ->method('readCsvLine')
            ->willReturnOnConsecutiveCalls(
                ['propertyName', 'value'],['file.orgs','bulk'], false, ['header1', 'header2', 'header3', 'header4'], ['value1', 'value2', 'value3', 'value4']
            );

        $file
            ->method('getContents')
            ->willReturn(json_encode([]));

        return $file;
    }
}

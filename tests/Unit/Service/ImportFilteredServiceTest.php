<?php


namespace oat\OneRoster\Tests\Unit\Service;


use oat\OneRoster\Exceptions\InvalidOptionException;
use oat\OneRoster\File\FileHandler;
use oat\OneRoster\Service\ImportFilteredService;
use PHPUnit\Framework\TestCase;

class ImportFilteredServiceTest extends TestCase
{

    const EXAMPLE_DATASET = 'example';
    const CORRECT_DATASET = 'data/samples/correct';

    /**
     * @throws \Exception
     */
    public function testFilterMultiple()
    {
        $filter = new ImportFilteredService(new FileHandler());

        $result = $filter->filterMultiple(realpath('data/samples/correct'));

        $this->assertIsArray($result);
        $this->assertNotEmpty($result);

        $this->assertArrayHasKey('classes', $result);
        $this->assertArrayHasKey('courses', $result);
        $this->assertArrayHasKey('demographics', $result);
        $this->assertArrayHasKey('enrollments', $result);
        $this->assertArrayHasKey('orgs', $result);
        $this->assertArrayHasKey('users', $result);

        $this->assertNotEmpty($filter->getEntitiesModes(), 'Entities modes not resolved');

        $this->assertEmpty($filter->getErrors(), 'Errors in dataset found');
        $this->assertTrue($filter->getIsDatasetValid(), 'Dataset not valid');
        $this->assertTrue($filter->getIsHeadersCorrect(), 'Headers not correct');
    }

    /**
     * @throws \Exception
     */
    public function testFilterMultipleOnlyClasses()
    {
        $filter = new ImportFilteredService(new FileHandler(), ['classes']);
        $result = $filter->filterMultiple(realpath('data/samples/correct'));

        $this->assertIsArray($result);
        $this->assertArrayHasKey('classes', $result);
        $this->assertArrayNotHasKey('courses', $result);
        $this->assertArrayNotHasKey('demographics', $result);
        $this->assertArrayNotHasKey('enrollments', $result);
        $this->assertArrayNotHasKey('orgs', $result);
        $this->assertArrayNotHasKey('users', $result);

        $this->assertNotEmpty($filter->getEntitiesModes(), 'Entities modes not resolved');

        $this->assertEmpty($filter->getErrors(), 'Errors in dataset found');
        $this->assertTrue($filter->getIsDatasetValid(), 'Dataset not valid');
        $this->assertTrue($filter->getIsHeadersCorrect(), 'Headers not correct');
    }

    /**
     * @throws \Exception
     */
    public function testFilterUppercaseHeaders()
    {
        $filter = (new ImportFilteredService(new FileHandler()));

        $result = $filter->filterMultiple(realpath('data/samples/uppercase_headers'));

        $this->assertNotEmpty($result, 'Result is empty');

        $this->assertArrayHasKey('enrollments', $result, 'Enrollments file not resolved');
        $this->assertNotEmpty($result['enrollments'], 'Empty enrollments array');

        $this->assertNotEmpty($filter->getEntitiesModes(), 'Entities modes not resolved');

        $this->assertEmpty($filter->getErrors(), 'Errors in dataset found');
        $this->assertTrue($filter->getIsDatasetValid(), 'Dataset not valid');
        $this->assertFalse($filter->getIsHeadersCorrect(), 'Headers resolved as correct');
    }

    /**
     * @throws \Exception
     */
    public function testMissedFiles()
    {
        $filter = (new ImportFilteredService(new FileHandler()));

        $result = $filter->filterMultiple(realpath('data/samples/missed_files'));

        $this->assertNotEmpty($filter->getErrors(), 'Should be errors');
        $this->assertArrayHasKey('manifest', $filter->getErrors(), 'Should be errors under manifest key');
        $this->assertFalse($filter->getIsDatasetValid(), 'Dataset should not be valid');
    }

    /**
     * @throws \Exception
     */
    public function testDuplicateSourcedId()
    {
        $filter = (new ImportFilteredService(new FileHandler()));

        $result = $filter->filterMultiple(realpath('data/samples/duplicate_sourcedid'));

        $this->assertNotEmpty($result, 'Result is empty!');
        $this->assertArrayHasKey('enrollments', $filter->getErrors(), 'Should have errors here');

        $filter->setDuplicateDeterminers([
            'enrollments' => function ($firstRecord, $lastRecord) {
                return $firstRecord['primary'] !== $lastRecord['primary'] && $firstRecord['primary'] === true;
            }
        ]);

        $result = $filter->filterMultiple(realpath('data/samples/duplicate_sourcedid'));

        $this->assertNotEmpty($result);
        $this->assertEmpty($filter->getErrors());
        $this->assertArrayNotHasKey('enrollments', $filter->getErrors());
    }

    /**
     * @throws \Exception
     */
    public function testFilterBrokenHeaders()
    {
        $filter = (new ImportFilteredService(new FileHandler()))
            ->setRulesToSkip([
                'users' => [
                    function ($row) {
                        return empty($row['enabledUser']);
                    }
                ]
            ]);

        $result = $filter->filterMultiple(realpath('data/samples/broken_headers'));

        $this->assertNotEmpty($result, 'Result is empty!');

        $this->assertArrayHasKey('enrollments', $result); //todo
        $this->assertArrayHasKey('classes', $result);
        $this->assertArrayHasKey('courses', $result);
        $this->assertArrayHasKey('demographics', $result);
        $this->assertArrayHasKey('orgs', $result);
        $this->assertArrayHasKey('users', $result);

        $this->assertIsArray($filter->getEntitiesModes());
        $this->assertNotEmpty($filter->getEntitiesModes());

        $this->assertFalse($filter->getIsDatasetValid(), 'Dataset is valid!');
        $this->assertNotEmpty($filter->getErrors(), 'Errors is empty!');

        $this->assertTrue($filter->getIsHeadersCorrect(), 'Headers are not correct!'); //todo
    }

    public function testFilterBrokenOptions()
    {
        $filter = (new ImportFilteredService(new FileHandler()));

        $this->expectException(InvalidOptionException::class);
        $rows = $filter->filterMultiple(realpath('data/samples/correct'), ['version' => '']);
    }

    /**
     * @throws \Exception
     */
    public function testFilterUsingRulesToSkip()
    {
        $filter = (new ImportFilteredService(new FileHandler()))
            ->setRulesToSkip([
                'users' => [
                    function ($currentRecord) {
                        return $currentRecord['enabledUser'] === false;
                    }
                ]
            ]);

        $result = $filter->filterMultiple(realpath('data/samples/correct'));

        $this->assertNotEmpty($filter->getSkippedEntriesByEntities(), 'No entries was skipped!');

        $this->assertIsArray($result);
        $this->assertArrayHasKey('enrollments', $result);

        $this->assertIsArray($filter->getEntitiesModes());
        $this->assertNotEmpty($filter->getEntitiesModes());

        $this->assertEmpty($filter->getErrors());
        $this->assertTrue($filter->getIsDatasetValid());
        $this->assertTrue($filter->getIsHeadersCorrect());
    }
}
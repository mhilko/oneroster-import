<?php

namespace oat\OneRoster\Tests\Unit\File;

use oat\OneRoster\Exceptions\NotAvailableFileException;
use oat\OneRoster\File\FileHandler;
use PHPUnit\Framework\TestCase;

class FileHandlerTest extends TestCase
{

    protected const TEST_DATA = [1, 'test', 123, 'test data'];

    protected $testFileName;

    protected function setUp(): void
    {
        $this->testFileName = tempnam(sys_get_temp_dir(), 'test');

        $file = fopen($this->testFileName, 'w+');
        fputcsv($file, self::TEST_DATA);
        fclose($file);
    }

    public function testOpen()
    {
        $fileHandler = new FileHandler();

        $this->assertIsResource($fileHandler->open($this->testFileName));

        $this->expectException(NotAvailableFileException::class);

        $fileHandler->open('/path/to/not/existed/file');
    }

    protected function tearDown(): void
    {
        unlink($this->testFileName);
    }

}

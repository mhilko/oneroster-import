<?php

namespace oat\OneRoster\Tests\Unit\Entity\Factory;

use Doctrine\Common\Collections\ArrayCollection;
use oat\OneRoster\Entity\EntityInterface;
use oat\OneRoster\Entity\Factory\EntityFactory;
use oat\OneRoster\Entity\Manifest;
use oat\OneRoster\Entity\Organisation;
use oat\OneRoster\Entity\RelationConfig;
use oat\OneRoster\Exceptions\InvalidEntityClassException;
use oat\OneRoster\Exceptions\WrongEntityProvidedException;
use oat\OneRoster\Storage\StorageInterface;
use PHPUnit\Framework\TestCase;

class EntityFactoryTest extends TestCase
{

    /**
     * @throws \Exception
     * @throws \ReflectionException
     */
    public function testCreateCollection()
    {
        $collection = EntityFactory::createCollection(
            Organisation::class,
            $this->mockStorage(),
            $this->mockRelationConfig()
        );

        $this->assertInstanceOf(ArrayCollection::class, $collection);
    }

    /**
     * @throws \Exception
     * @throws \ReflectionException
     */
    public function testCreateCollectionWithInResults()
    {
        $inResults  = new ArrayCollection([
            'orgs' => [
                'sourcedId' => '12345'
            ]
        ]);

        $collection = EntityFactory::createCollection(
            Organisation::class,
            $this->mockStorage(),
            $this->mockRelationConfig(),
            $inResults
        );

        $this->assertInstanceOf(ArrayCollection::class, $collection);
    }

    /**
     * @throws \Exception
     * @throws \ReflectionException
     */
    public function testCreate()
    {
        $entity = EntityFactory::create(
            '12345',
            Organisation::class,
            $this->mockStorage(),
            $this->mockRelationConfig()
        );

        $this->assertInstanceOf(EntityInterface::class, $entity);
    }

    /**
     * @throws \ReflectionException|InvalidEntityClassException|WrongEntityProvidedException
     */
    public function testCreateEntityNonValidClass()
    {
        $this->expectException(InvalidEntityClassException::class);

        EntityFactory::create(
            '12345',
            'some/non/entity/class',
            $this->mockStorage(),
            $this->mockRelationConfig()
        );
    }

    /**
     * @throws \ReflectionException|InvalidEntityClassException|WrongEntityProvidedException
     */
    public function testCreateCollectionNonValidClass()
    {
        $this->expectException(InvalidEntityClassException::class);

        $collection = EntityFactory::createCollection(
            'some/non/entity/class',
            $this->mockStorage(),
            $this->mockRelationConfig()
        );
    }


    /**
     * @throws \ReflectionException|InvalidEntityClassException|WrongEntityProvidedException
     */
    public function testCreateWrongEntity()
    {
        $this->expectException(WrongEntityProvidedException::class);

        EntityFactory::create(
            '12345',
            \stdClass::class,
            $this->mockStorage(),
            $this->mockRelationConfig()
        );
    }

    /**
     * @throws \ReflectionException|InvalidEntityClassException|WrongEntityProvidedException
     */
    public function testCreateWrongEntityCollection()
    {
        $this->expectException(WrongEntityProvidedException::class);

        $collection = EntityFactory::createCollection(
            \stdClass::class,
            $this->mockStorage(),
            $this->mockRelationConfig()
        );
    }

    /**
     * @return StorageInterface
     * @throws \ReflectionException
     */
    protected function mockStorage(): StorageInterface
    {
        $storage = $this->getMockForAbstractClass(StorageInterface::class);

        $storage
            ->method('findByType')
            ->willReturn([
                'orgs' => [
                    'sourcedId' => '12345'
                ]
            ]);

        return $storage;
    }

    /**
     * @return RelationConfig
     */
    protected function mockRelationConfig(): RelationConfig
    {
        return $this->getMockBuilder(RelationConfig::class)->disableOriginalConstructor()->getMock();
    }
}

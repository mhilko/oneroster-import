<?php

namespace oat\OneRoster\Tests\Unit\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use oat\OneRoster\Entity\EntityInterface;
use oat\OneRoster\Entity\EntityRepository;
use oat\OneRoster\Entity\Manifest;
use oat\OneRoster\Entity\Organisation;
use oat\OneRoster\Entity\RelationConfig;
use oat\OneRoster\Service\ImportService;
use oat\OneRoster\Storage\StorageInterface;
use PHPUnit\Framework\TestCase;

class EntityRepositoryTest extends TestCase
{
    /**
     * @throws \ReflectionException
     */
    public function testGet()
    {
        $repo = new EntityRepository($this->mockStorage(), $this->mockRelationConfig('sourcedId'));

        $this->assertInstanceOf(EntityInterface::class, $repo->get('school_id', Organisation::class));
    }

    /**
     * @throws \ReflectionException
     */
    public function testGetAll()
    {
        $repo = new EntityRepository($this->mockStorage(), $this->mockRelationConfig('sourcedId'));

        $this->assertInstanceOf(ArrayCollection::class, $repo->getAll(Organisation::class));
    }

    /**
     * @throws \ReflectionException
     */
    public function testGetEntityMode()
    {
        $repo = new EntityRepository(
            $this->mockStorage(),
            $this->mockRelationConfig('sourcedId'),
            $this->mockManifest(Organisation::getType(), ImportService::ENTITY_MODE_DELTA)
        );

        $this->assertEquals(ImportService::ENTITY_MODE_DELTA, $repo->getEntityMode(Organisation::getType()));
        $this->assertEquals(ImportService::ENTITY_MODE_ABSENT, $repo->getEntityMode('unexpectedEntityType'));
    }

    /**
     * @throws \ReflectionException
     */
    public function testGetEntityModeWithoutManifest()
    {
        $repo = new EntityRepository(
            $this->mockStorage(),
            $this->mockRelationConfig('sourcedId')
        );

        $this->assertEquals(null, $repo->getEntityMode(Organisation::getType()));
        $this->assertEquals(null, $repo->getEntityMode('unexpectedEntityType'));
    }

    /**
     * @return StorageInterface
     * @throws \ReflectionException
     */
    protected function mockStorage(): StorageInterface
    {
        $storage = $this->getMockForAbstractClass(StorageInterface::class);

        $storage
            ->method('findByType')
            ->willReturn(new ArrayCollection([
                    'orgs' => [
                        'sourcedId' => 'school_id'
                    ],
                    'enrollments' => [
                        'sourcedId' => 'enrollment_id'
                    ],
                    'users' => [
                        'sourcedId' => 'user_id'
                    ],
                    'classes' => [
                        'sourcedId' => 'class_id'
                    ],
                ])
            );

        $storage
            ->method('findByTypeAndId')
            ->willReturn(new ArrayCollection([
                    'orgs' => [
                        'sourcedId' => 'org_id',
                    ],
                ])
            );

        return $storage;
    }

    /**
     * @param string $index
     * @return RelationConfig
     */
    protected function mockRelationConfig(string $index): RelationConfig
    {
        $relation = $this->getMockBuilder(RelationConfig::class)->disableOriginalConstructor()->getMock();

        $relation
            ->method('getConfig')
            ->willReturn($index);

        return $relation;
    }

    /**
     * @param string $entityType
     * @param string $mode
     * @return Manifest
     */
    protected function mockManifest(string $entityType, string $mode): Manifest
    {
        return  $this
            ->getMockBuilder(Manifest::class)
            ->setConstructorArgs([[$entityType => $mode]])
            ->setMethodsExcept(['getEntityMode'])
            ->getMock();
    }
}

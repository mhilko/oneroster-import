<?php

namespace oat\OneRoster\Tests\Unit\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use oat\OneRoster\Entity\RelationConfig;
use oat\OneRoster\Entity\User;
use oat\OneRoster\Storage\StorageInterface;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{

    /**
     * @throws \ReflectionException
     */
    public function testGetOrgs()
    {
        $entity = $this->getEntity('sourcedId');

        $this->assertInstanceOf(ArrayCollection::class, $entity->getOrgs());
    }

    /**
     * @throws \ReflectionException
     */
    public function testGetDemographics()
    {
        $entity = $this->getEntity('sourcedId');

        $this->assertInstanceOf(ArrayCollection::class, $entity->getDemographics());
    }

    /**
     * @throws \ReflectionException
     */
    public function testGetEnrollments()
    {
        $entity = $this->getEntity('sourcedId');

        $this->assertInstanceOf(ArrayCollection::class, $entity->getEnrollments());
    }

    public function testGetType()
    {
        $this->assertEquals('users', User::getType());
    }

    /**
     * @param string $index
     * @return User
     * @throws \ReflectionException
     */
    protected function getEntity(string $index): User
    {
        $obj = new User();
        $obj->setId('user_id');
        $obj->setStorage($this->mockStorage());
        $obj->setRelationConfig($this->mockRelationConfig($index));

        return $obj;
    }

    /**
     * @return StorageInterface
     * @throws \ReflectionException
     */
    protected function mockStorage(): StorageInterface
    {
        $storage = $this->getMockForAbstractClass(StorageInterface::class);

        $storage
            ->method('findByType')
            ->willReturn(new ArrayCollection([
                    'orgs' => [
                        'sourcedId' => 'school_id'
                    ],
                    'enrollments' => [
                        'sourcedId' => 'enrollment_id'
                    ],
                    'users' => [
                        'sourcedId' => 'user_id'
                    ],
                    'classes' => [
                        'sourcedId' => 'class_id'
                    ],
                ])
            );

        $storage
            ->method('findByTypeAndId')
            ->willReturn(new ArrayCollection([
                    'orgs' => [
                        'sourcedId' => 'org_id',
                    ],
                ])
            );

        return $storage;
    }

    /**
     * @param string $index
     * @return RelationConfig
     */
    protected function mockRelationConfig(string $index): RelationConfig
    {
        $relation = $this->getMockBuilder(RelationConfig::class)->disableOriginalConstructor()->getMock();

        $relation
            ->method('getConfig')
            ->willReturn($index);

        return $relation;
    }
}

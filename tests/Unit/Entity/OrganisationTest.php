<?php

namespace oat\OneRoster\Tests\Unit\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use oat\OneRoster\Entity\Organisation;
use oat\OneRoster\Entity\RelationConfig;
use oat\OneRoster\Storage\StorageInterface;
use PHPUnit\Framework\TestCase;

class OrganisationTest extends TestCase
{

    /**
     * @throws \ReflectionException
     */
    public function testGetClasses()
    {
        $entity = $this->getEntity('sourcedId');

        $this->assertInstanceOf(ArrayCollection::class, $entity->getClasses());
    }

    /**
     * @throws \ReflectionException
     */
    public function testGetUsers()
    {
        $entity = $this->getEntity('sourcedId');

        $this->assertInstanceOf(ArrayCollection::class, $entity->getUsers());
    }

    /**
     * @throws \ReflectionException
     */
    public function testGetEnrollments()
    {
        $entity = $this->getEntity('sourcedId');

        $this->assertInstanceOf(ArrayCollection::class, $entity->getEnrollments());
    }

    public function testGetType()
    {
        $this->assertEquals('orgs', Organisation::getType());
    }

    /**
     * @param string $index
     * @return Organisation
     * @throws \ReflectionException
     */
    protected function getEntity(string $index): Organisation
    {
        $obj = new Organisation();
        $obj->setId('org_id');
        $obj->setStorage($this->mockStorage());
        $obj->setRelationConfig($this->mockRelationConfig($index));

        return $obj;
    }

    public function testGetData()
    {
        $entity = $this->getEntity('sourcedId');

        $data = $entity->getData();

        if ($data instanceof ArrayCollection) {
            $data = $data->toArray();
        }

        $this->assertIsArray($data);
        $this->assertNotEmpty($data);
    }

    /**
     * @return StorageInterface
     * @throws \ReflectionException
     */
    protected function mockStorage(): StorageInterface
    {
        $storage = $this->getMockForAbstractClass(StorageInterface::class);

        $storage
            ->method('findByType')
            ->willReturn(new ArrayCollection([
                    'orgs' => [
                        'sourcedId' => 'school_id'
                    ],
                    'enrollments' => [
                        'sourcedId' => 'enrollment_id'
                    ],
                    'users' => [
                        'sourcedId' => 'user_id'
                    ],
                    'classes' => [
                        'sourcedId' => 'class_id'
                    ],
                ])
            );

        $storage
            ->method('findByTypeAndId')
            ->willReturn(new ArrayCollection([
                    'orgs' => [
                        'sourcedId' => 'org_id',
                    ],
                ])
            );

        return $storage;
    }

    /**
     * @param string $index
     * @return RelationConfig
     */
    protected function mockRelationConfig(string $index): RelationConfig
    {
        $relation = $this->getMockBuilder(RelationConfig::class)->disableOriginalConstructor()->getMock();

        $relation
            ->method('getConfig')
            ->willReturn($index);

        return $relation;
    }
}

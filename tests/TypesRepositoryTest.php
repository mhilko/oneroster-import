<?php

namespace oat\OneRoster\Tests;

use oat\OneRoster\Exceptions\UnknownEntityTypeException;
use oat\OneRoster\TypesRepository;
use PHPUnit\Framework\TestCase;

class TypesRepositoryTest extends TestCase
{
    public function testGetFileName()
    {
        $entitiesFilesByNames = [
            'manifest' => 'manifest.json',
            'categories' => 'categories.json',
            'classResources' => 'classResources.json',
            'courseResources' => 'courseResources.json',
            'orgs' => 'orgs.json',
            'classes' => 'classes.json',
            'users' => 'users.json',
            'enrollments' => 'enrollments.json',
            'lineItems' => 'lineItems.json',
            'courses' => 'courses.json',
            'academicSessions' => 'academicSessions.json',
            'demographics' => 'demographics.json',
            'resources' => 'resources.json',
            'results' => 'results.json',
        ];

        foreach ($entitiesFilesByNames as $entityName => $fileName) {
            $receivedFileName = TypesRepository::getFileName($entityName);

            $this->assertEquals($fileName, $receivedFileName);
        }
    }

    public function testWrongTypeException()
    {
        $this->expectException(UnknownEntityTypeException::class);

        TypesRepository::getFileName('notExistedEntity');
    }
}

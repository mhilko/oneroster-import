<?php


namespace oat\OneRoster\Service;

use Doctrine\Common\Collections\ArrayCollection;
use oat\OneRoster\Exceptions\InvalidOptionException;
use oat\OneRoster\File\FileHandler;
use oat\OneRoster\Import\ImporterFactory;

abstract class AbstractImportService
{

    public const ENTITY_MODE_ABSENT = 'absent';
    public const ENTITY_MODE_BULK = 'bulk';
    public const ENTITY_MODE_DELTA = 'delta';

    /**
     * @var array Default CSV controls
     */
    protected $options = [
        'version' => 'v1.1',
        'csvControl' => [
            'delimiter' => ',',
            'enclosure' => '"',
            'escape' => '\\',
        ]
    ];

    /**
     * @var FileHandler
     */
    protected $fileHandler;

    /**
     * @var string
     */
    protected $pathToFolder;

    /**
     * @var null|array
     */
    private $entitiesModes;

    /**
     * AbstractService constructor.
     * @param $fileHandler
     */
    public function __construct(FileHandler $fileHandler)
    {
        $this->fileHandler = $fileHandler;
    }

    /**
     * @return string
     */
    public function getPathToFolder(): string
    {
        return $this->pathToFolder;
    }

    /**
     * @param string $pathToFolder
     */
    public function setPathToFolder(string $pathToFolder)
    {
        $this->pathToFolder = $pathToFolder;
    }

    /**
     * @param array|ArrayCollection $manifestData
     * @return array
     */
    protected function getAvailableTypesFromResult($manifestData): array
    {
        $entitiesModes = [];

        if (!empty($manifestData)) {
            foreach ($manifestData as ['propertyName' => $property, 'value' => $value]) {
                if (strpos($property, 'file.') !== false && $value !== self::ENTITY_MODE_ABSENT) {
                    $parts = explode('.', $property);
                    $type = end($parts);
                    $entitiesModes[$type] = $value;
                }
            }
        }

        $this->entitiesModes = $entitiesModes;

        return array_keys($entitiesModes);
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getEntitiesModes(): array
    {
        if ($this->entitiesModes === null) {
            $result = $this->importFile($this->getPathToFolder() . 'manifest.csv', 'manifest', $this->options);

            $types = [];

            foreach ($result as $row) {
                $property = $row['propertyName'];
                $value    = $row['value'];

                if (strpos($property, 'file.') !== false && $value !== 'absent') {
                    $parts = explode('.', $property);
                    $type = end($parts);
                    $types[$type] = $value;
                }
            }

            $this->entitiesModes = $types;
        }

        return $this->entitiesModes;
    }

    protected function importFile(string $file, string $type, array $options = []): ArrayCollection
    {
        $options = array_merge($this->options, $options);
        $this->validateOptions($options);

        $importer = (new ImporterFactory($options['version'], $this->fileHandler))->build($type);
        $fileResource = $this->fileHandler->open($file);

        $lines  = [];
        $header = [] ;

        [$delimiter, $enclosure, $escape] = array_values($options['csvControl']);
        $index = 0;
        while (is_array($line = $this->fileHandler->readCsvLine($fileResource, 0, $delimiter, $enclosure, $escape))) {
            $index++;
            $dataLine = array_map('trim', $line);
            if ($index === 1) {
                $header = $dataLine;
                continue;
            }
            $lines[] = $dataLine;
        }

        return $importer->import($header, $lines);
    }

    /**
     * @param string $entityName
     * @return string|null
     * @throws \Exception
     */
    public function getEntityMode(string $entityName): string
    {
        return $this->getEntitiesModes()[$entityName] ?? self::ENTITY_MODE_ABSENT;
    }

    /**
     * @param array $options
     * @throws InvalidOptionException
     */
    protected function validateOptions(array $options): void
    {
        $requiredOptions = ['version', 'csvControl'];

        foreach ($requiredOptions as $requiredOption) {
            if (empty($options[$requiredOption])) {
                throw new InvalidOptionException($requiredOption);
            }
        }
    }
}

<?php


namespace oat\OneRoster\Service;

use oat\OneRoster\Entity\EntityInterface;
use oat\OneRoster\Entity\EntityRepository;
use oat\OneRoster\Entity\Factory\RelationConfigFactory;
use oat\OneRoster\Entity\Manifest;
use oat\OneRoster\File\FileHandler;
use oat\OneRoster\Storage\StorageInterface;
use oat\OneRoster\TypesRepository;

class ExportService
{
    /**
     * @var EntityRepository
     */
    private $entityRepository;

    /**
     * @var string|null
     */
    private $systemName;

    /**
     * @var string|null
     */
    private $systemCode;

    /**
     * ExportService constructor.
     * @param EntityRepository $entityRepository
     * @param string|null $systemName
     * @param string|null $systemCode
     */
    public function __construct(
        EntityRepository $entityRepository,
        ?string $systemName = null,
        ?string $systemCode = null
    ) {
        $this->entityRepository = $entityRepository;
        $this->systemCode = $systemCode;
        $this->systemName = $systemName;
    }

    /**
     * @param StorageInterface $storage
     * @param string|null $systemName
     * @param string|null $systemCode
     * @return static
     */
    public static function createFromStorage(
        StorageInterface $storage,
        ?string $systemName = null,
        ?string $systemCode = null,
        array $manifestEntitiesModes = []
    ): self {
        $fileHandler = new FileHandler();
        $relationConfig = (new RelationConfigFactory($fileHandler))->create();

        $entityRepository = new EntityRepository($storage, $relationConfig, new Manifest($manifestEntitiesModes));

        return new self($entityRepository, $systemName, $systemCode);
    }

    /**
     * @param string $directoryPath
     * @throws \Exception
     */
    public function saveToDirectory(string $directoryPath): void
    {
        $possibleEntities = TypesRepository::getEntitiesList();

        $parsedEntities = [];

        foreach ($possibleEntities as $fileName => $possibleEntity) {
            $entitiesFromRepository = $this->entityRepository->getAll($possibleEntity);

            if ($entitiesFromRepository->count() === 0) {
                continue;
            }

            $parsedEntities[$fileName] = $entitiesFromRepository->map(function (EntityInterface $entity) {
                $dataRow = $entity->getData();

                foreach ($dataRow as $key => $value) {
                    if (is_bool($value)) {
                        $value = $value ? 'true' : 'false';

                        if ($key === 'dateLastModified') {
                            $value = null;
                        }

                        $dataRow[$key] = $value;
                    } elseif ($value instanceof \DateTime) {
                        $dataRow[$key] = $value->format(\DateTime::ISO8601);
                    }
                }

                return $dataRow;
            })->toArray();
        }

        $existedEntities = array_keys($parsedEntities);
        $parsedEntities['manifest'] = $this->generateManifestData($existedEntities);

        foreach ($parsedEntities as $entityName => $data) {
            $fileName = $entityName . '.csv';
            $fileStream = fopen($directoryPath . DIRECTORY_SEPARATOR . $fileName, 'w');

            $data = array_filter($data);

            $firstRow = $data[0] ?? [];
            $headers = array_keys($firstRow);
            array_unshift($data, $headers);

            foreach ($data as $dataRow) {
                fputcsv($fileStream, $dataRow);
            }

            fclose($fileStream);
        }
    }

    /**
     * @param array $providedEntities array of provided entities (to determine which is absent or bulk)
     *                                could be passed as associative array where value is entity status
     *                                (for example ['users' => 'delta'])
     *
     * @return array|string[]         associative array of manifest data
     */
    private function generateManifestData(array $providedEntities): array
    {
        $possibleEntities = array_keys(TypesRepository::getList());

        $manifestData = [
            'manifest.version'      => '1',
            'oneroster.version'     => '1.1',
        ];

        if ($this->systemName) {
            $manifestData['source.systemName'] = $this->systemName;
        }

        if ($this->systemCode) {
            $manifestData['source.systemCode'] = $this->systemCode;
        }

        foreach ($possibleEntities as $possibleEntity) {
            $isEntityProvided = in_array($possibleEntity, $providedEntities)
                || array_key_exists($possibleEntity, $providedEntities);

            $entityStatus = $this->entityRepository->getEntityMode($possibleEntity) ?? $providedEntities[$possibleEntity]
                ?? ($isEntityProvided ? 'delta' : 'absent');

            $manifestData['file.' . $possibleEntity] = $entityStatus;
        }

        $manifestContent = [];

        foreach ($manifestData as $propertyName => $value) {
            $manifestContent[] = ['propertyName' => $propertyName, 'value' => $value];
        }

        return $manifestContent;
    }
}


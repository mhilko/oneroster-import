<?php

namespace oat\OneRoster\Service;

use oat\OneRoster\File\FileHandler;
use oat\OneRoster\Filter\FilterFactory;
use oat\OneRoster\Helpers\HeadersHelper;
use Doctrine\Common\Collections\ArrayCollection;
use oat\OneRoster\TypesRepository;

class ImportFilteredService extends AbstractImportService
{

    /**
     * contains entities errors, like ['entity' => [...errors]]
     *
     * @var array
     */
    private $errors = [];

    /**
     * will be false if dataset couldn't be processed
     *
     * @var bool
     */
    private $isDatasetValid = true;

    /**
     * will be false is headers different from existed
     *
     * @var bool
     */
    private $isHeadersCorrect = true;

    /**
     * list of entities which should be filtered
     *
     * @var array
     */
    private $onlyEntities;

    /**
     * @var array
     */
    private $rulesToSkip = [];

    /**
     * @var array
     */
    private $duplicateDeterminers = [];

    /**
     * @var array
     */
    private $skippedEntriesByEntity = [];

    public function __construct(FileHandler $fileHandler, array $onlyEntities = [])
    {
        parent::__construct($fileHandler);

        $this->onlyEntities = $onlyEntities;
    }

    /**
     * set array of rules to skip as closures
     * function will receive one argument - currentEntity, and should return boolean
     * if function return true, current record will be skipped in filter processing
     *
     * @param array $rules like [ 'entity' => [ function($currentRecord), ... ], ... ]
     * @return $this
     */
    public function setRulesToSkip(array $rules): self
    {
        $this->rulesToSkip = $rules;

        return $this;
    }

    /**
     * set array of duplicate determiners as closures
     * function will receive two arguments - existedRecord and currentRecord, and should return boolean state of
     * duplicate (true if it duplicate and it should be skipped, or false to overwrite existedRecord).
     * If error thrown it will be recognized as error in validator
     *
     * @param array $determiners like [ 'entity' => function($existedRecord, $currentRecord), ... ]
     * @return $this
     */
    public function setDuplicateDeterminers(array $determiners): self
    {
        $this->duplicateDeterminers = $determiners;

        return $this;
    }

    /**
     * add error to errors array
     *
     * @param string $entityType type of the entity (for example manifest, orgs)
     * @param string $message error message
     */
    private function addError(string $entityType, string $message): void
    {
        if (!array_key_exists($entityType, $this->errors)) {
            $this->errors[$entityType] = [];
        }

        $this->errors[$entityType][] = ['line' => null, 'message' => $message];
    }

    /**
     * add errors array (for example, from validator)
     *
     * @param string $entityType type of the entity (for example manifest, orgs)
     * @param array $errors array of errors, like [['line' => '', 'message' => '']...]
     */
    private function addErrors(string $entityType, array $errors): void
    {
        if (empty($errors)) {
            return;
        }

        $this->errors[$entityType] = array_merge($this->errors[$entityType] ?? [], $errors);
    }

    /**
     * get list of errors
     *
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @return bool
     */
    public function getIsDatasetValid(): bool
    {
        return $this->isDatasetValid;
    }

    /**
     * @return bool
     */
    public function getIsHeadersCorrect(): bool
    {
        return $this->isHeadersCorrect;
    }

    /**
     * @param $pathToFolder
     * @param $options
     * @return array
     * @throws \Exception
     */
    public function filterMultiple(string $pathToFolder, array $options = [])
    {
        $this->errors = [];
        $options = array_merge($this->options, $options);
        $this->validateOptions($options);

        $results = [];

        $pathToFolder = realpath($pathToFolder) . '/';

        $availableTypes = $this->getAvailableTypesFromResult(
            $this->filter($pathToFolder . 'manifest.csv', 'manifest', $options)
        );

        foreach ($availableTypes as $availableType) {
            if (!empty($this->onlyEntities) && !in_array($availableType, $this->onlyEntities)) {
                continue;
            }

            $results[$availableType] = $this->filter($pathToFolder . $availableType . '.csv', $availableType, $options);
        }

        $results = array_filter($results);

        return $results;
    }

    /**
     * @param string $file
     * @param string $type
     * @param array $options
     * @return ArrayCollection|null
     * @throws \Exception
     */
    public function filter(string $file, string $type, array $options = []): ?ArrayCollection
    {
        $options = array_merge($this->options, $options);
        $this->validateOptions($options);

        try {
            $fileResource = $this->fileHandler->open($file);
        } catch (\Exception $e) {
            $this->addError('manifest', "{$type} file is missed.");
            $this->isDatasetValid = false;

            return null;
        }

        $lines = [];
        $headers = [];

        [$delimiter, $enclosure, $escape] = array_values($options['csvControl']);

        $index = 0;

        while (is_array($line = $this->fileHandler->readCsvLine($fileResource, 0, $delimiter, $enclosure, $escape))) {
            $index++;
            $dataLine = array_map(function($value) {
                return str_replace('"', '', trim($value));
            }, $line);
            if ($index === 1) {
                $headers = $dataLine;
                continue;
            }

            if (empty(array_filter($line, 'trim'))) {
                continue;
            }

            $lines[] = $dataLine;
        }

        if (empty($lines)) {
            $this->addError($type, 'File has no any rows to proceed');

            if (in_array($type, TypesRepository::REQUIRED_ENTITIES)) {
                $this->isDatasetValid = false;
            }

            return null;
        }

        $filter = (new FilterFactory($this->fileHandler, $this->options['version']))
            ->build($type)
            ->setRulesToSkip($this->getRulesToSkip($type))
            ->setDuplicateDeterminer($this->getDuplicateDeterminer($type));

        $newHeaders = HeadersHelper::makeCorrectHeadersForType($headers, $type);

        if (HeadersHelper::hasCorrectHeaders($newHeaders, $type) === false) {
            $this->addError($type, 'File doesn\'t contain correct headers!');

            if (in_array($type, TypesRepository::REQUIRED_ENTITIES)) {
                $this->isDatasetValid = false;
            }

            return null;
        }

        $arrayLinesCount = array_count_values(array_map(function($row) {
            return count($row);
        }, $lines));

        $lineColumns = max(array_keys($arrayLinesCount));
        $headerColumns = count($newHeaders);

        if ($lineColumns > $headerColumns) {
            $this->addError($type, 'The headers do not match the number of columns');

            if (in_array($type, TypesRepository::REQUIRED_ENTITIES)) {
                $this->isDatasetValid = false;
            }

            return null;
        }

        if ($lineColumns !== $headerColumns) {
            $lines = array_map(function (array $row) use ($headerColumns) {
                return array_pad($row, $headerColumns, '');
            }, $lines);
        }

        if ((bool)count(array_diff($headers, $newHeaders))) {
            $this->isHeadersCorrect = false;
        }

        $lines = array_filter($lines, function ($row) {
            return !empty(array_filter($row, 'trim'));
        });

        $validLines = $filter->getValidated($newHeaders, $lines);

        if ($filter->getSkippedEntitiesCount()) {
            $this->skippedEntriesByEntity[$type] = $filter->getSkippedEntitiesCount();
        }

        $this->addErrors($type, $filter->getErrors());

        if ($this->isDatasetValid) {
            $this->isDatasetValid = $filter->getIsDatasetValid();
        }

        return $validLines;
    }

    /**
     * @param string $type
     * @return array|\Closure[]
     */
    private function getRulesToSkip(string $type): array
    {
        return $this->rulesToSkip[$type] ?? [];
    }

    /**
     * @param string $type
     * @return \Closure|null function with two arguments - oldRecord and currentRecord, which return boolean
     *                       state of duplicate (true if it duplicate and it should be skipped, or false to overwrite
     *                       oldRecord). If error thrown it will be recognized as error in validator
     */
    private function getDuplicateDeterminer(string $type): ?\Closure
    {
        return $this->duplicateDeterminers[$type] ?? null;
    }

    /**
     * @return array
     */
    public function getSkippedEntriesByEntities(): array
    {
        return $this->skippedEntriesByEntity;
    }
}

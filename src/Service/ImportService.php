<?php

namespace oat\OneRoster\Service;

use Doctrine\Common\Collections\ArrayCollection;
use oat\OneRoster\File\FileHandler;
use oat\OneRoster\Import\ImporterFactory;

class ImportService extends AbstractImportService
{
    /**
     * import one file as entity list
     *
     * @param string $file path to file
     * @param string $type entity name
     * @param array $options array of options
     * @return ArrayCollection
     * @throws \Exception
     */
    public function import(string $file, string $type, array $options = []): ArrayCollection
    {
        return $this->importFile($file, $type, $options);
    }

    /**
     * @param $pathToFolder
     * @param $options
     * @return array
     * @throws \Exception
     */
    public function importMultiple($pathToFolder, array $options = [])
    {
        $options = array_merge($this->options, $options);
        $this->validateOptions($options);
        $results = [];

        $manifest = $this->import($pathToFolder . 'manifest.csv', 'manifest', $options);

        $availableTypes = $this->getAvailableTypesFromResult($manifest);

        foreach ($availableTypes as $availableType) {
            $results[$availableType] = $this->import($pathToFolder . $availableType . '.csv', $availableType, $options);
        }

        return $results;
    }
}

<?php

namespace oat\OneRoster\Helpers;

use oat\OneRoster\Exceptions\UnknownEntityTypeException;
use oat\OneRoster\TypesRepository;

class HeadersHelper
{
    /**
     * return list of possible dataType headers
     *
     * @param string $dataType
     * @return array
     * @throws UnknownEntityTypeException
     */
    public static function getFileHeadersForType(string $dataType): array
    {
        $dataTypeFile = TypesRepository::getFileName($dataType);

        $pathToSchemaJson = realpath(__DIR__ . '/../../config/v1.1/') . '/' . $dataTypeFile;

        $fileContent = file_get_contents($pathToSchemaJson);
        $dataTypeSchema = json_decode($fileContent, true);

        $headers = [];

        foreach ($dataTypeSchema as ['columnId' => $columnId]) {
            $headers[] = $columnId;
        }

        return $headers;
    }

    /**
     * return list of correct headers
     *
     * @param array $possibleHeaders
     * @param string $dataType
     * @return array
     * @throws UnknownEntityTypeException
     */
    public static function makeCorrectHeadersForType(array $possibleHeaders, string $dataType): array
    {
        $correctHeaders = self::getFileHeadersForType($dataType);
        $correctHeadersLowercase = array_map(function ($item) {
            return strtolower($item);
        }, $correctHeaders);
        $correctHeadersMap = array_combine($correctHeadersLowercase, $correctHeaders);
        $resultHeaders = [];

        foreach ($possibleHeaders as $possibleHeader) {
            $possibleHeaderLowercase = strtolower($possibleHeader);
            $resultHeaders[] = $correctHeadersMap[$possibleHeaderLowercase] ?? $possibleHeader;
        }

        return $resultHeaders;
    }

    /**
     * @param array $existedHeaders
     * @param string $dataType
     * @return bool
     * @throws UnknownEntityTypeException
     */
    public static function hasCorrectHeaders(array $existedHeaders, string $dataType): bool
    {
        $correctHeaders = self::getFileHeadersForType($dataType);
        $correctHeadersLowercase = array_map(function ($item) {
            return strtolower($item);
        }, $correctHeaders);

        $correctHeadersMap = array_combine($correctHeadersLowercase, $correctHeaders);

        foreach ($existedHeaders as $existedHeader) {
            $existedHeaderLowercase = strtolower($existedHeader);

            if (array_key_exists($existedHeaderLowercase, $correctHeadersMap)) {
                return true;
            }
        }

        return false;
    }
}

<?php


namespace oat\OneRoster\Exceptions;


class InvalidOptionException extends \Exception
{
    public function __construct(string $option)
    {
        parent::__construct(sprintf('%s should be specified as option', $option));
    }
}
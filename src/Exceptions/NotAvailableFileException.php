<?php


namespace oat\OneRoster\Exceptions;


class NotAvailableFileException extends \Exception
{
    public function __construct(string $file)
    {
        parent::__construct(sprintf('File %s cannot be loaded', $file));
    }
}
<?php


namespace oat\OneRoster\Exceptions;


class WrongEntityProvidedException extends \Exception
{
    public function __construct(string $entityType)
    {
        parent::__construct(sprintf('Invalid entity provided: %s', $entityType));
    }
}
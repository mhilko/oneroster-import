<?php

namespace oat\OneRoster\Exceptions;

class RequiredException extends \Exception
{
    public function __construct(string $field)
    {
        parent::__construct(sprintf('Required field %s does not exists or it is empty', $field));
    }
}
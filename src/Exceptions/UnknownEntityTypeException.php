<?php


namespace oat\OneRoster\Exceptions;


class UnknownEntityTypeException extends \Exception
{
    public function __construct(string $entityType)
    {
        parent::__construct(sprintf('Requested entity %s does not have known file name', $entityType));
    }
}
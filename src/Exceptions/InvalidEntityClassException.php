<?php


namespace oat\OneRoster\Exceptions;


class InvalidEntityClassException extends \Exception
{
    public function __construct(string $entityClass)
    {
        parent::__construct(sprintf('%s not a valid class', $entityClass));
    }
}
<?php

namespace oat\OneRoster\Schema;

use oat\OneRoster\Exceptions\FormatException;
use oat\OneRoster\Exceptions\RequiredException;

class Validator
{
    private const DATE_FORMAT = 'Y-m-d';
    private const DATE_TIME_FORMAT = 'Y-m-d H:i:s';

    private $schema;

    /**
     * Validator constructor.
     * @param array $schema
     */
    public function __construct(array $schema)
    {
        $this->schema = $schema;
    }

    /**
     * @param array $dataRow
     * @return array
     * @throws RequiredException
     * @throws FormatException
     */
    public function validate(array $dataRow): array
    {
        $this->validateRequiredFields($dataRow);
        return $this->validateFormat($dataRow);
    }

    /**
     * @param array $dataRow
     * @throws RequiredException
     */
    protected function validateRequiredFields(array $dataRow): void
    {
        $requiresFields = $this->extractRequiresFields();

        foreach ($requiresFields as $requiredField) {
            $fieldValue = $dataRow[$requiredField] ?? null;

            if (empty($fieldValue) && $fieldValue !== "0") {
                throw new RequiredException($requiredField);
            }
        }
    }

    /**
     * @param array $dataRow
     * @return array validated $dataRow
     * @throws FormatException
     */
    protected function validateFormat(array $dataRow): array
    {
        foreach ($this->schema as $itemSchema) {
            $columnIdentifier = $itemSchema['columnId'];
            if (!isset($dataRow[$columnIdentifier])){
                continue;
            }

            $format = $itemSchema['format'];
            $value = $dataRow[$columnIdentifier];

            if ($format === 'boolean') {
                $value = filter_var($value, FILTER_VALIDATE_BOOLEAN);
            }

            if ($format === 'date' || $format === 'datetime') {
                $dateFormats = [\DateTime::ISO8601, static::DATE_TIME_FORMAT, static::DATE_FORMAT, 'm/d/y'];
                $oldValue = $value;

                foreach ($dateFormats as $dateFormat) {
                    $value = \DateTime::createFromFormat($dateFormat, $oldValue);

                    if ($value !== false) {
                        break;
                    }
                }

                if ($value === false && $this->isFieldRequired($columnIdentifier)) {
                    throw FormatException::create($columnIdentifier, $format, gettype($value));
                }

            } else {
                if (gettype($value) !== $format && $this->isFieldRequired($columnIdentifier)) {
                    throw FormatException::create($columnIdentifier, $format, gettype($value));
                }
            }

            $dataRow[$columnIdentifier] = $value;
        }

        return $dataRow;
    }

    /**
     * @return array<string>
     */
    protected function extractRequiresFields(): array
    {
        $required = [];

        foreach ($this->schema as $item) {
            if ($item['required'] === true) {
                $required[] = $item['columnId'];
            }
        }

        return $required;
    }

    /**
     * @param string $field
     * @return bool
     */
    protected function isFieldRequired(string $field): bool
    {
        $requiresFields = $this->extractRequiresFields();

        return in_array($field, $requiresFields, true);
    }

    /**
     * @return array
     * @codeCoverageIgnore
     */
    public function __debugInfo()
    {
        return $this->schema;
    }
}

<?php


namespace oat\OneRoster;

use oat\OneRoster\Entity\AcademicSession;
use oat\OneRoster\Entity\Category;
use oat\OneRoster\Entity\ClassResource;
use oat\OneRoster\Entity\ClassRoom;
use oat\OneRoster\Entity\Course;
use oat\OneRoster\Entity\CourseResource;
use oat\OneRoster\Entity\Demographic;
use oat\OneRoster\Entity\Enrollment;
use oat\OneRoster\Entity\LineItem;
use oat\OneRoster\Entity\Organisation;
use oat\OneRoster\Entity\Resource;
use oat\OneRoster\Entity\Result;
use oat\OneRoster\Entity\User;
use oat\OneRoster\Exceptions\UnknownEntityTypeException;

class TypesRepository
{
    private static $types = [
        'manifest' => 'manifest.json',
        'categories' => 'categories.json',
        'classResources' => 'classResources.json',
        'courseResources' => 'courseResources.json',
        'orgs' => 'orgs.json',
        'classes' => 'classes.json',
        'users' => 'users.json',
        'enrollments' => 'enrollments.json',
        'lineItems' => 'lineItems.json',
        'courses' => 'courses.json',
        'academicSessions' => 'academicSessions.json',
        'demographics' => 'demographics.json',
        'resources' => 'resources.json',
        'results' => 'results.json',
    ];

    private static $typesEntities = [
        'academicSessions' => AcademicSession::class,
        'categories' => Category::class,
        'classes' => ClassRoom::class,
        'classResources' => ClassResource::class,
        'courses' => Course::class,
        'courseResources' => CourseResource::class,
        'demographics' => Demographic::class,
        'enrollments' => Enrollment::class,
        'lineItems' => LineItem::class,
        'orgs' => Organisation::class,
        'resources' => Resource::class,
        'results' => Result::class,
        'users' => User::class
    ];

    public const REQUIRED_ENTITIES = ['classes', 'courses', 'enrollments', 'orgs', 'users'];

    /**
     * return list of filenames by entity types
     *
     * @codeCoverageIgnore
     * @return array<string> [entityType => entityFileName, ...]
     */
    public static function getList(): array
    {
        return self::$types;
    }

    /**
     * return list of entity classes by entity types
     *
     * @codeCoverageIgnore
     * @return array<string> [entityType => entityClass, ...]
     */
    public static function getEntitiesList(): array
    {
        return self::$typesEntities;
    }

    /**
     * return entity filename by type
     *
     * @param string $type entity type
     * @return string file name for type
     * @throws UnknownEntityTypeException
     */
    public static function getFileName(string $type): string
    {
        if (!isset(self::$types[$type])) {
            throw new UnknownEntityTypeException($type);
        }

        return self::$types[$type];
    }
}

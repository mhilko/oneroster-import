<?php

namespace oat\OneRoster\File;

use oat\OneRoster\Exceptions\NotAvailableFileException;

class FileHandler
{
    /**
     * @param string $filePath
     * @param string $mode
     * @return bool|resource
     * @throws \Exception
     */
    public function open(string $filePath, string $mode = 'r')
    {
        if (!file_exists($filePath) || !is_readable($filePath) ||
            ($fileHandler = fopen($filePath, $mode)) === false) {
            throw new NotAvailableFileException($filePath);
        }

        return $fileHandler;
    }

    /**
     * @param $filename
     * @return bool|string
     */
    public function getContents($filename)
    {
        return file_get_contents($filename);
    }

    /**
     * @param $handle
     * @param int $length
     * @param string $delimiter
     * @param string $enclosure
     * @param string $escape
     * @return array|false|null
     */
    public function readCsvLine($handle, $length = 0, $delimiter = ',', $enclosure = '"', $escape = '\\')
    {
        return fgetcsv($handle, $length, $delimiter, $enclosure, $escape);
    }
}
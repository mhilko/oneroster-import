<?php

namespace oat\OneRoster\Storage;

use Doctrine\Common\Collections\ArrayCollection;

interface StorageInterface
{
    /**
     * @param string $typeOfEntity [orgs,classes..]
     *
     * @return ArrayCollection
     */
    public function findByType(string $typeOfEntity);

    /**
     * @param string $typeOfEntity  [orgs,classes..]
     *
     * @param string $id
     * @return array
     */
    public function findByTypeAndId(string $typeOfEntity, string $id);
}
<?php

namespace oat\OneRoster\Storage;

use Doctrine\Common\Collections\ArrayCollection;
use oat\OneRoster\Service\ImportService;

class CsvStorage implements StorageInterface
{
    /** @var ImportService */
    private $importService;

    /** @var array */
    private $imports;

    /**
     * CsvStorage constructor.
     * @param ImportService $importService
     */
    public function __construct(ImportService $importService)
    {
        $this->importService = $importService;
    }

    /**
     * @param string $typeOfEntity [orgs,classes..]
     *
     * @return ArrayCollection|array
     * @throws \Exception
     */
    public function findByType(string $typeOfEntity)
    {
        if (!isset($this->imports[$typeOfEntity])) {
            $this->imports[$typeOfEntity] = $this->importEntities($typeOfEntity);
        }

        return $this->imports[$typeOfEntity];
    }

    /**
     * @param string $typeOfEntity [orgs,classes..]
     *
     * @param $id
     * @return array
     * @throws \Exception
     */
    public function findByTypeAndId(string $typeOfEntity, string $id)
    {
        if (!isset($this->imports[$typeOfEntity])) {
            $this->imports[$typeOfEntity] = $this->importEntities($typeOfEntity);
        }

        return $this->imports[$typeOfEntity]->get($id);
    }

    private function importEntities(string $entityName)
    {
        return $this->importService->import($this->importService->getPathToFolder() . $entityName . '.csv', $entityName);
    }
}
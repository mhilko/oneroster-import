<?php


namespace oat\OneRoster\Filter;

use Doctrine\Common\Collections\ArrayCollection;
use oat\OneRoster\Schema\Validator as SchemaValidator;

class Filter implements FilterInterface
{
    /**
     * @var SchemaValidator
     */
    private $schemaValidator;

    /**
     * @var array
     */
    private $errors = [];

    /**
     * @var array
     */
    private $rulesToSkip;

    /**
     * @var bool
     */
    private $isDatasetValid = true;

    /**
     * @var null|\Closure
     */
    private $duplicateDeterminer;

    /**
     * @var int
     */
    private $skippedCount = 0;

    /**
     * Filter constructor.
     * @param SchemaValidator $validator
     * @param array $rulesToSkip
     */
    public function __construct(SchemaValidator $validator, array $rulesToSkip = [])
    {
        $this->schemaValidator = $validator;
        $this->rulesToSkip = $rulesToSkip;
    }

    /**
     * @param array $rulesToSkip
     * @return Filter
     */
    public function setRulesToSkip(array $rulesToSkip = []): FilterInterface
    {
        $this->rulesToSkip = $rulesToSkip;

        return $this;
    }

    /**
     * @param null|\Closure $closure callback which accept two arguments - existedRecord and currentRecord,
     *                               should return true if currentRecord is duplicate, otherwise false to rewrite
     *                               existedRecord
     * @return Filter
     */
    public function setDuplicateDeterminer(?\Closure $closure): FilterInterface
    {
        $this->duplicateDeterminer = $closure;

        return $this;
    }

    /**
     * @param array $header
     * @param array $data
     * @return ArrayCollection
     */
    public function getValidated(array $header, array $data): ArrayCollection
    {
        $result = new ArrayCollection();

        $duplicateDeterminer = $this->duplicateDeterminer;

        foreach ($data as $index => $row) {
            $rowWitHeader = array_combine($header, $row);

            $skipRecord = false;

            try {
                $rowWitHeader = $this->schemaValidator->validate($rowWitHeader);
            } catch (\Exception $e) {
                $this->addError($index, $e->getMessage());
                $skipRecord = true;
            }

            foreach ($this->rulesToSkip as $rule) {
                if ($skipRecord || $rule instanceof \Closure === false) {
                    continue;
                }

                $skipRecord = $rule($rowWitHeader);
            }

            if ($skipRecord) {
                $this->skippedCount++;
                continue;
            }

            $sourcedId = $rowWitHeader['sourcedId'] ?? null;

            if ($sourcedId === null) {
                $result->add($rowWitHeader);
                continue;
            }

            if ($result->containsKey($sourcedId)) {
                $isItError = true;

                if ($duplicateDeterminer && $duplicateDeterminer instanceof \Closure) {
                    try {
                        // will return true if current record is duplicate and should be skipped
                        // if return false - current record should overwrite old one
                        $isDuplicate = $duplicateDeterminer($result->get($sourcedId), $rowWitHeader);

                        $this->skippedCount++;

                        if ($isDuplicate) {
                            continue;
                        } else {
                            $isItError = false;
                        }
                    } catch (\Exception $e) {
                        // if catch error - do nothing, it should be recognized as error
                    }
                }

                if ($isItError) {
                    $this->addError($index, "Entity with sourcedId {$sourcedId} already exist.");
                    $this->isDatasetValid = false;
                    continue;
                }
            }

            $result->set($sourcedId, $rowWitHeader);
        }

        return $result;
    }

    /**
     * @return int
     */
    public function getSkippedEntitiesCount(): int
    {
        return $this->skippedCount;
    }

    /**
     * @param int $line
     * @param string $message
     */
    private function addError(int $line, string $message): void
    {
        $this->errors[] = ['line' => $line + 2, 'message' => $message];
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @return bool
     */
    public function getIsDatasetValid(): bool
    {
        return $this->isDatasetValid;
    }
}

<?php

namespace oat\OneRoster\Filter;

use Doctrine\Common\Collections\ArrayCollection;

interface FilterInterface
{
    public function getValidated(array $header, array $data): ArrayCollection;

    public function getErrors(): ?array;

    public function getIsDatasetValid(): bool;

    public function setRulesToSkip(array $rulesToSkip): self;

    public function setDuplicateDeterminer(?\Closure $closure): self;

    public function getSkippedEntitiesCount(): int;
}

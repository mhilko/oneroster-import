<?php


namespace oat\OneRoster\Filter;

use oat\OneRoster\Exceptions\UnknownEntityTypeException;
use oat\OneRoster\File\FileHandler;
use oat\OneRoster\Schema\Validator;
use oat\OneRoster\TypesRepository;

class FilterFactory
{
    /**
     * @var string
     */
    private $version;

    /**
     * @var FileHandler
     */
    private $fileHandler;

    /**
     * @var array
     */
    private $types;

    /**
     * FactoryImporter constructor.
     * @param FileHandler $fileHandler
     * @param string $version
     * @param array $types
     * [key => value] key = type of import, value = the json schema for validation
     */
    public function __construct(FileHandler $fileHandler, string $version = 'v1.1', array $types = [])
    {
        $this->version = $version;
        $this->fileHandler = $fileHandler;
        $this->types = array_merge(TypesRepository::getList(), $types);
    }

    /**
     * @param string $type
     * @return FilterInterface
     * @throws UnknownEntityTypeException
     */
    public function build(string $type): FilterInterface
    {
        if (!array_key_exists($type, $this->types)) {
            throw new UnknownEntityTypeException($type);
        }

        $pathToSchemaJson = __DIR__ . '/../../config/' . $this->version . '/' . $this->types[$type];
        $schema = json_decode($this->fileHandler->getContents($pathToSchemaJson), true);

        return new Filter(new Validator($schema));
    }
}

<?php

namespace oat\OneRoster\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use oat\OneRoster\Entity\Factory\EntityFactory;
use oat\OneRoster\Storage\StorageInterface;

abstract class AbstractEntity implements EntityInterface
{
    /** @var string */
    protected $id;

    /** @var StorageInterface */
    protected $storage;

    /** @var RelationConfig */
    protected $relationConfig;

    /** @return  string */

    /**
     * @return array|ArrayCollection
     */
    public function getData()
    {
        return $this->storage->findByTypeAndId(static::getType(), $this->id);
    }

    /**
     * @param StorageInterface $storage
     */
    public function setStorage(StorageInterface $storage): void
    {
        $this->storage = $storage;
    }

    /**
     * @param RelationConfig $relationConfig
     */
    public function setRelationConfig(RelationConfig $relationConfig): void
    {
        $this->relationConfig = $relationConfig;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param null|string $id
     */
    public function setId(?string $id): void
    {
        $this->id = $id;
    }

    /**
     * @param string $className
     * @param bool $inLineIds
     * @return Collection
     * @throws \Exception
     */
    protected function getChildrenRelationEntities(string $className, bool $inLineIds = false): Collection
    {
        $keyType   = $className::getType();
        $entities  = $this->storage->findByType($keyType);
        $index     = $this->relationConfig->getConfig(static::getType() . '.relations.' . $keyType . '.index');
        $id        = $this->id;

        if ($inLineIds) {
            $results = $entities->filter(function ($entity) use ($index, $id) {
                $identifier = $entity[$index] ?? '';
                $identifiersArray = explode(',', $identifier);
                $identifiersArray = array_map('trim', $identifiersArray);

                return in_array($id, $identifiersArray);
            });
        } else {
            $results = $entities->matching(Criteria::create()->where(Criteria::expr()->eq($index, $id)));
        }

        return EntityFactory::createCollection($className, $this->storage, $this->relationConfig, $results);
    }

    /**
     * @param $className
     * @param bool $inLineIds
     * @return \Doctrine\Common\Collections\ArrayCollection|EntityInterface
     * @throws \Exception
     */
    protected function getParentRelationEntity($className, $inLineIds = false)
    {
        $keyType   = $className::getType();
        $entities  = $this->storage->findByType($keyType);
        $index     = $this->relationConfig->getConfig(static::getType() . '.relations.' . $keyType . '.index');

        if ($inLineIds){
            $valueOfId = $this->getData()[$index];
            $criteria  = Criteria::create()->where(Criteria::expr()->in('sourcedId', explode(',', $valueOfId)));
            $results   = $entities->matching($criteria);

            return EntityFactory::createCollection($className, $this->storage, $this->relationConfig, $results);
        }

        $valueOfId = $this->getData()[$index];

        return EntityFactory::create($valueOfId, $className, $this->storage, $this->relationConfig);
    }

    /**
     * @return array
     * @codeCoverageIgnore
     */
    public function __debugInfo(): array
    {
        return $this->getData();
    }
}
<?php


namespace oat\OneRoster\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use oat\OneRoster\Entity\Factory\EntityFactory;
use oat\OneRoster\Storage\StorageInterface;

class EntityRepository
{
    /** @var StorageInterface */
    private $storage;

    /** @var RelationConfig */
    private $relationConfig;

    /**
     * @var Manifest|null
     */
    private $manifest;

    /**
     * EntityManager constructor.
     * @param StorageInterface $storage
     * @param RelationConfig $relationConfig
     * @param Manifest|null $manifest
     */
    public function __construct(StorageInterface $storage, RelationConfig $relationConfig, ?Manifest $manifest = null)
    {
        $this->storage = $storage;
        $this->relationConfig = $relationConfig;
        $this->manifest = $manifest;
    }

    /**
     * @param string $id
     * @param string $entityName
     * @return EntityInterface
     * @throws \Exception
     */
    public function get(string $id, string $entityName): EntityInterface
    {
        return EntityFactory::create($id, $entityName, $this->storage, $this->relationConfig);
    }

    /**
     * @param string $entityName
     * @return ArrayCollection
     * @throws \Exception
     */
    public function getAll(string $entityName): ArrayCollection
    {
        return EntityFactory::createCollection($entityName, $this->storage, $this->relationConfig);
    }

    /**
     * @param string $entityName
     * @return string entity mode (bulk, delta or absent) or null if no manifest provided
     */
    public function getEntityMode(string $entityName): ?string
    {
        if ($this->manifest === null) {
            return null;
        }

        return $this->manifest->getEntityMode($entityName);
    }
}
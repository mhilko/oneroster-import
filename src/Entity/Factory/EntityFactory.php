<?php

namespace oat\OneRoster\Entity\Factory;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use oat\OneRoster\Entity\EntityInterface;
use oat\OneRoster\Entity\RelationConfig;
use oat\OneRoster\Exceptions\InvalidEntityClassException;
use oat\OneRoster\Exceptions\WrongEntityProvidedException;
use oat\OneRoster\Storage\StorageInterface;

class EntityFactory
{
    /**
     * @param $entityName
     * @param StorageInterface $storage
     * @param RelationConfig $relationConfig
     * @param Collection|null $inResults
     * @return ArrayCollection
     * @throws InvalidEntityClassException|WrongEntityProvidedException
     */
    public static function createCollection(
        $entityName,
        StorageInterface $storage,
        RelationConfig $relationConfig,
        ?Collection $inResults = null
    ): ArrayCollection
    {
        if (!class_exists($entityName)){
            throw new InvalidEntityClassException($entityName);
        }

        $obj = new $entityName();

        if (!$obj instanceof EntityInterface) {
            throw new WrongEntityProvidedException($entityName);
        }

        $allObjs = new ArrayCollection();

        $allResults = $inResults === null ? $storage->findByType($obj::getType()) : $inResults;

        if (!empty($allResults)) {
            foreach ($allResults as $result) {
                $id = $result['sourcedId'];
                /** @var EntityInterface $objNew */
                $objNew = new $entityName();
                $objNew->setId($id);
                $objNew->setStorage($storage);
                $objNew->setRelationConfig($relationConfig);

                $allObjs->add($objNew);
            }
        }

        return $allObjs;
    }

    /**
     * @param $id
     * @param $entityName
     * @param StorageInterface $storage
     * @param RelationConfig $relationConfig
     * @return EntityInterface
     * @throws InvalidEntityClassException|WrongEntityProvidedException
     */
    public static function create(
        $id,
        $entityName,
        StorageInterface $storage,
        RelationConfig $relationConfig
    ): EntityInterface {
        if (!class_exists($entityName)){
            throw new InvalidEntityClassException($entityName);
        }

        $obj = new $entityName();

        if (!$obj instanceof EntityInterface) {
            throw new WrongEntityProvidedException($entityName);
        }

        $obj->setId($id);
        $obj->setStorage($storage);
        $obj->setRelationConfig($relationConfig);

        return $obj;
    }
}
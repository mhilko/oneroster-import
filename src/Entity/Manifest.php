<?php


namespace oat\OneRoster\Entity;


use oat\OneRoster\Service\AbstractImportService;

class Manifest
{

    private $version;

    private $systemName;

    private $systemCode;

    private $files;

    /**
     * Manifest constructor.
     *
     * @param array $files
     * @param string $version
     * @param string $systemName
     * @param string $systemCode
     */
    public function __construct(array $files, string $version = '1', string $systemName = '', string $systemCode = '')
    {
        $this->version = $version;
        $this->systemName = $systemName;
        $this->systemCode = $systemCode;
        $this->files = $files;
    }

    /**
     * @param string $entity
     * @return string
     */
    public function getEntityMode(string $entity): string
    {
        return $this->files[$entity] ?? AbstractImportService::ENTITY_MODE_ABSENT;
    }

}
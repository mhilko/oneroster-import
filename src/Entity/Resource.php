<?php

namespace oat\OneRoster\Entity;

class Resource extends AbstractEntity
{
    /** @return  string */
    static public function getType(): string
    {
       return 'resources';
    }
}
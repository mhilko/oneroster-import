<?php


namespace oat\OneRoster\Entity;


use oat\OneRoster\Storage\StorageInterface;

interface EntityInterface
{
    /**
     * @return array
     */
    public function getData();

    /**
     * @param string|null $id
     */
    public function setId(?string $id): void;

    /**
     * @param StorageInterface $storage
     */
    public function setStorage(StorageInterface $storage): void;

    /**
     * @param RelationConfig $relationConfig
     */
    public function setRelationConfig(RelationConfig $relationConfig): void;


    /** @return string */
    public static function getType(): string;
}